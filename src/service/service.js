const termDate = (out) => { 
  const timeDiff = new Date (out) - new Date ()
   
  const days = Math.ceil(timeDiff / (1000 * 3600 * 24))

  return days >= 0
}

export default {
  termDate
}